# ECU Day of Infamy 1941-2001 Digital Exhibit

Jekyll site for the ECU Day of Infamy 1941-2001 legacy digital exhibit.
* [http://digital.lib.ecu.edu/pearlharbor](http://digital.lib.ecu.edu/pearlharbor)

## Modification

Modification requires building the site with [Jekyll](https://jekyllrb.com/), which requires Ruby.

1. Clone repo
2. $ cd pearlharbor
3. $ bundle install
4. $ bundle exec jekyll serve

This should make [http://localhost:4000/pearlharbor/](http://localhost:4000/pearlharbor/) available in your browser to view the site (trailing slash is important here). After changes are made, copy the contents of the _site directory to the live webserver.

